package com.xsis.pos260.repositories;

import com.xsis.pos260.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepo extends JpaRepository<Product, Long> {
}
