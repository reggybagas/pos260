package com.xsis.pos260.repositories;

import com.xsis.pos260.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;

//ini dibuat untuk mengakses fungsi jpa repo
public interface CategoryRepo extends JpaRepository<Category, Long> {

}
