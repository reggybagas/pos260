package com.xsis.pos260.repositories;

import com.xsis.pos260.models.Variant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VariantRepo extends JpaRepository<Variant, Long> {
}
