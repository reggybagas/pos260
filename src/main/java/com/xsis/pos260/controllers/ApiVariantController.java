package com.xsis.pos260.controllers;

import com.xsis.pos260.models.Category;
import com.xsis.pos260.models.Variant;
import com.xsis.pos260.repositories.CategoryRepo;
import com.xsis.pos260.repositories.VariantRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiVariantController {

    @Autowired
    private VariantRepo variantRepo;

    @GetMapping("/variant")
    public ResponseEntity<List<Variant>> GetAllVariant(){
        try {
            List<Variant> variant = this.variantRepo.findAll();
            return new ResponseEntity<>(variant, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //get by id
    @GetMapping("/variant/{id}")
    public ResponseEntity<List<Variant>> GetVariantById(@PathVariable("id") Long id) {
        try {
            //optional berarti data yang dicari bisa null
            Optional<Variant> variant = this.variantRepo.findById(id);
            if (variant.isPresent()) {
                ResponseEntity rest = new ResponseEntity(variant, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(value = "/variant")
    //response entity untuk return message success atau tidak
    public ResponseEntity<Object> saveVariant(@RequestBody Variant variant) {
        try {
            variant.setCreatedBy("Yoshie");
            variant.setCreatedOn(new Date());
            this.variantRepo.save(variant);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/variant/{id}")
    public ResponseEntity<Object> UpdateVariant(@RequestBody Variant variant, @PathVariable("id") Long id) {
        Optional<Variant> variantOptional = this.variantRepo.findById(id);
        try {
            if (variantOptional.isPresent()) {
                variant.setId(id);
                variant.setModifiedBy("Yoshie");
                variant.setModifiedOn(new Date());
                this.variantRepo.save(variant);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/variant/{id}")
    public ResponseEntity<Object> DeleteVariant(@RequestBody Variant variant, @PathVariable("id") Long id) {
        Optional<Variant> variantOptional = this.variantRepo.findById(id);
        try {
            if (variantOptional.isPresent()) {
                variant.setId(id);
                variant.setDelete(true);
                variant.setModifiedBy("Yoshie");
                variant.setModifiedOn(new Date());
                this.variantRepo.save(variant);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

}
