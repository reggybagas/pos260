package com.xsis.pos260.controllers;

import com.xsis.pos260.models.Product;
import com.xsis.pos260.models.Variant;
import com.xsis.pos260.repositories.ProductRepo;
import com.xsis.pos260.repositories.VariantRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiProductController {

    @Autowired
    private ProductRepo productRepo;

    @GetMapping("/product")
    public ResponseEntity<List<Product>> GetAllProduct(){
        try {
            List<Product> product = this.productRepo.findAll();
            return new ResponseEntity<>(product, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //get by id
    @GetMapping("/product/{id}")
    public ResponseEntity<List<Product>> GetProductById(@PathVariable("id") Long id) {
        try {
            //optional berarti data yang dicari bisa null
            Optional<Product> product = this.productRepo.findById(id);
            if (product.isPresent()) {
                ResponseEntity rest = new ResponseEntity(product, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(value = "/product")
    //response entity untuk return message success atau tidak
    public ResponseEntity<Object> saveProduct(@RequestBody Product product) {
        try {
            product.setCreatedBy("Yoshie");
            product.setCreatedOn(new Date());
            this.productRepo.save(product);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/product/{id}")
    public ResponseEntity<Object> UpdateProduct(@RequestBody Product product, @PathVariable("id") Long id) {
        Optional<Product> productOptional = this.productRepo.findById(id);
        try {
            if (productOptional.isPresent()) {
                product.setId(id);
                product.setModifiedBy("Yoshie");
                product.setModifiedOn(new Date());
                this.productRepo.save(product);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/product/{id}")
    public ResponseEntity<Object> DeleteProduct(@RequestBody Product product, @PathVariable("id") Long id) {
        Optional<Product> productOptional = this.productRepo.findById(id);
        try {
            if (productOptional.isPresent()) {
                product.setId(id);
                product.setDelete(true);
                product.setModifiedBy("Yoshie");
                product.setModifiedOn(new Date());
                this.productRepo.save(product);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

}
