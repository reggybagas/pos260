package com.xsis.pos260.controllers;

import com.xsis.pos260.models.Category;
import com.xsis.pos260.repositories.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

//untuk akses ke repo interface
@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiCategoryController {

    @Autowired
    private CategoryRepo categoryRepo;

    @GetMapping("/category")
    public  ResponseEntity<List<Category>> GetAllCategory(){
        try {
            List<Category> category = this.categoryRepo.findAll();
            return new ResponseEntity<>(category, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //get by id
    @GetMapping("/category/{id}")
    public ResponseEntity<List<Category>> GetCategoryById(@PathVariable("id") Long id) {
        try {
            //optional berarti data yang dicari bisa null
            Optional<Category> category = this.categoryRepo.findById(id);
            if (category.isPresent()) {
                ResponseEntity rest = new ResponseEntity(category, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(value = "/category")
    //response entity untuk return message success atau tidak
    public ResponseEntity<Object> saveCategory(@RequestBody Category category) {
        try {
            category.setCreatedBy("Yoshie");
            category.setCreatedOn(new Date());
            this.categoryRepo.save(category);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/category/{id}")
    public ResponseEntity<Object> UpdateCategory(@RequestBody Category category, @PathVariable("id") Long id) {
        Optional<Category> categoryOptional = this.categoryRepo.findById(id);
        try {
            if (categoryOptional.isPresent()) {
                category.setId(id);
                category.setModifiedBy("Yoshie");
                category.setModifiedOn(new Date());
                this.categoryRepo.save(category);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/category/{id}")
    public ResponseEntity<Object> DeleteCategory(@RequestBody Category category, @PathVariable("id") Long id) {
        Optional<Category> categoryOptional = this.categoryRepo.findById(id);
        try {
            if (categoryOptional.isPresent()) {
                category.setId(id);
                category.setDelete(true);
                category.setModifiedBy("Yoshie");
                category.setModifiedOn(new Date());
                this.categoryRepo.save(category);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

}
